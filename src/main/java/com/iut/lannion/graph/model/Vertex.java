package com.iut.lannion.graph.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.ArrayList;
import java.util.List;

import org.locationtech.jts.geom.Coordinate;

/**
 * 
 * Un sommet dans un graphe
 * 
 * @author etassel
 *
 */
public class Vertex {

	/**
	 * Identifiant du sommet
	 */
	private String id;

	/**
	 * Position du sommet
	 */
	private Coordinate coordinate;
	
	@JsonIgnore
	private List<Edge> inEdges;
	
	@JsonIgnore
	private List<Edge> outEdges;

	protected Vertex() {
		this.inEdges = new ArrayList<>();
		this.outEdges = new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Coordinate getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(Coordinate coordinate) {
		this.coordinate = coordinate;
	}
	
	public List<Edge> getInEdges() {
		return this.inEdges;
	}

	public void setInEdges(List<Edge> inEdges) {
		this.inEdges = inEdges;
	}

	public List<Edge> getOutEdges() {
		return this.outEdges;
	}

	public void setOutEdges(List<Edge> outEdges) {
		this.outEdges = outEdges;
	}

	@Override
	public String toString() {
		return id;
	}

}
