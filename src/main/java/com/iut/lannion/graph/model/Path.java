package com.iut.lannion.graph.model;

import java.util.ArrayList;
import java.util.List;

public class Path {

	private List<Edge> edges;
	
	public Path() {
		this.edges = new ArrayList<>();
	}

	public double getLength() {
		double result = 0.0;
		
		for(Edge edge: this.edges)
			result += edge.getCost();
		
		return result;
	}
	
	public List<Edge> getEdges() {
		return edges;
	}

	public void setEdges(List<Edge> edges) {
		this.edges = edges;
	}	
	
}
