ackage com.iut.lannion.graph.routing;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.iut.lannion.graph.errors.NotFoundException;
import com.iut.lannion.graph.model.Edge;
import com.iut.lannion.graph.model.Graph;
import com.iut.lannion.graph.model.Path;
import com.iut.lannion.graph.model.PathTree;
import com.iut.lannion.graph.model.Vertex;

/**
 * 
 * Utilitaire pour le calcul du plus court chemin dans un graphe
 * 
 * @author etassel
 *
 */
public class DijkstraPathFinder {

	private static final Logger log = LogManager.getLogger(DijkstraPathFinder.class);

	private Graph graph;

	private PathTree pathTree;

	public DijkstraPathFinder(Graph graph) {
		this.graph = graph;
	}

	/**
	 * Calcul du plus court chemin entre une origine et une destination
	 * 
	 * @param origin
	 * @param destination
	 * @return
	 */
	public Path findPath(Vertex origin, Vertex destination) {
		log.info("findPath({},{})...", origin, destination);

		this.pathTree = new PathTree(origin);
		Vertex current;
		while ((current = findNextVertex()) != null) {
			visit(current);
			if (this.pathTree.isReached(destination)) {
				log.info("findPath({},{}) : path found", origin, destination);
				return this.pathTree.getPath(destination);
			}
		}

		log.info("findPath({},{}) : path not found", origin, destination);
		throw new NotFoundException(String.format("Path not found from '%s' to '%s'", origin, destination));
	}

	/**
	 * Parcourt les arcs sortants pour atteindre les sommets avec le meilleur coût
	 * 
	 * @param vertex
	 */
	private void visit(Vertex vertex) {
		log.trace("visit({})", vertex);
		List<Edge> outEdges = graph.getOutEdges(vertex);
		/*
		 * On étudie chacun des arcs sortant pour atteindre de nouveaux sommets ou
		 * mettre à jour des sommets déjà atteint si on trouve un meilleur coût
		 */
		for (Edge outEdge : outEdges) {
			Vertex reachedVertex = outEdge.getTarget();
			/*
			 * Convervation de arc permettant d'atteindre le sommet avec un meilleur coût
			 * sachant que les sommets non atteint ont pour coût "POSITIVE_INFINITY"
			 */
			double newCost = this.pathTree.getOrCreateNode(vertex).getCost() + outEdge.getCost();
			if (newCost < this.pathTree.getOrCreateNode(reachedVertex).getCost()) {
				this.pathTree.getOrCreateNode(reachedVertex).setCost(newCost);
				this.pathTree.getOrCreateNode(reachedVertex).setReachingEdge(outEdge);
			}
		}
		/*
		 * On marque le sommet comme visité
		 */
		this.pathTree.getOrCreateNode(vertex).setVisited(true);
	}

	/**
	 * Recherche le prochain sommet à visiter. Dans l'algorithme de Dijkstra, ce
	 * sommet est le sommet non visité le plus proche de l'origine du calcul de plus
	 * court chemin.
	 * 
	 * @return
	 */
	private Vertex findNextVertex() {
		double minCost = Double.POSITIVE_INFINITY;
		Vertex result = null;
		for (Vertex vertex : this.pathTree.getReachedVertices()) {
			// sommet déjà visité?
			if (this.pathTree.getOrCreateNode(vertex).isVisited()) {
				continue;
			}
			// sommet non atteint?
			if (this.pathTree.getOrCreateNode(vertex).getCost() == Double.POSITIVE_INFINITY) {
				continue;
			}

			// sommet le plus proche de la source?
			if (this.pathTree.getOrCreateNode(vertex).getCost() < minCost) {
				result = vertex;
			}
		}
		return result;
	}

}
